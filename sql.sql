ALTER TABLE ventas
ADD correlativo varchar(255) NULL after fecha,
ADD iva_retenido decimal(9,2) DEFAULT 0 after correlativo;

ALTER TABLE clientes
ADD dui varchar(255) NULL after nit;

ALTER TABLE ventas CHANGE estado estado VARCHAR(255) NOT NULL;

import { Component, OnInit, Input, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../services/alert.service';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.css']
})
export class DashComponent implements OnInit {

	@ViewChild('mcorte')
    public corteTemplate!: TemplateRef<any>;
    modalRef!: BsModalRef;

	public datos:any = {};
	public caja:any = {};
	public entrada:any = {};
	public salida:any = {};
	public gasto:any = {};
	public nota:any = {};
	public corte:any = {};
	public usuario:any = {};

	public select:string = '';
	

	public loading:boolean = false;

	constructor( 
	    private apiService: ApiService, private router: Router, private alertService: AlertService, 
        private modalService: BsModalService
	) { }

	ngOnInit() {
		    
		this.cargar();

	}

	openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    public cargar(){
		this.today();

		this.apiService.read('caja/', this.apiService.auth_user().caja_id).subscribe(caja => {
           this.caja = caja;
        }, error => {this.alertService.error(error); this.alertService.error(error); });

        this.entrada = {};
        this.entrada.fecha = this.apiService.date();
        this.entrada.usuario_id = this.apiService.auth_user().id;
        this.entrada.empresa_id = this.apiService.auth_user().empresa_id;
        this.salida = {};
        this.salida.fecha = this.apiService.date();
        this.salida.usuario_id = this.apiService.auth_user().id;
        this.salida.empresa_id = this.apiService.auth_user().empresa_id;
        this.gasto = {};
        this.gasto.fecha = this.apiService.date();
        this.gasto.usuario_id = this.apiService.auth_user().id;
        this.gasto.empresa_id = this.apiService.auth_user().empresa_id;
        this.nota = {};
        this.nota.estado = 'Pendiente';
        this.nota.fecha = this.apiService.date();
        this.nota.usuario_id = this.apiService.auth_user().id;
        this.nota.empresa_id = this.apiService.auth_user().empresa_id;
    }

    openModalCorte(template: TemplateRef<any>, corte:any) {
        this.corte = corte;
        this.modalRef = this.modalService.show(template, {class: 'modal-sm', backdrop: 'static', keyboard: false});
    }


    public onSubmitCorte() {
        this.loading = true;

        this.corte.estado = 'Abierta';
        this.corte.caja_id = this.caja.id;
        this.corte.usuario_id = this.apiService.auth_user().id;
        console.log(this.corte);
        this.loading = false;
        this.apiService.store('corte', this.corte).subscribe(corte => {
            this.corte = corte;
            this.alertService.success("Datos guardados");
            this.loading = false;
            this.today();
          this.modalRef?.hide();
        },error => {this.alertService.error(error.error); this.loading = false; });

    }

    public cerrarCaja() {
        this.loading = true;

        if (confirm('Confirma que desea cerrar el turno en caja')) { 
            this.corte.estado = 'Cerrada';
            this.apiService.store('corte-cierre', this.corte).subscribe(corte => {
                this.router.navigate(['/login']);
                this.alertService.success("Caja cerrada");
                this.loading = false;
              this.modalRef?.hide();
            },error => {this.alertService.error(error.error); this.loading = false; });
        }


    }

		private today(){

			this.select = 'today';
			this.apiService.getAll('dash/today').subscribe(datos => {
	           this.datos = datos;
	           this.corte = datos.corte;
	           this.caja = datos.caja;
	           if (this.corte == null || this.corte.estado == 'Cerrada') { 
	               console.log('Falta corte');
	               this.openModalCorte(this.corteTemplate, {});
	           }else{
	               this.datos = datos;
	           }
	        }, error => {this.alertService.error(error); this.alertService.error(error); });
		}

		private month(){
			this.select = 'month';
			this.apiService.getAll('dash/month').subscribe(datos => {
	           this.datos = datos;
	        }, error => {this.alertService.error(error); this.alertService.error(error); });
		}

		private year(){
			this.select = 'year';
			this.apiService.getAll('dash/year').subscribe(datos => {
	           this.datos = datos;
	        }, error => {this.alertService.error(error); this.alertService.error(error); });
		}

		private all(){
			this.select = 'all';
			this.apiService.getAll('dash/all').subscribe(datos => {
	           this.datos = datos;
	        }, error => {this.alertService.error(error); this.alertService.error(error); });
		}

	onSubmitEntrada() {
	    this.loading = true;
	    // Guardamos al gasto
	    this.apiService.store('entrada', this.entrada).subscribe(entrada => {
	        this.entrada = entrada;
	        this.cargar();
	        this.alertService.success("Entrada guardada");
	        this.loading = false;
	        this.modalRef?.hide();
	    },error => {
	        this.alertService.error(error.error);
	        this.loading = false;
	    });
	}

	onSubmitSalida() {
	    this.loading = true;
	    // Guardamos al gasto
	    this.apiService.store('salida', this.salida).subscribe(salida => {
	        this.salida = salida;
	        this.cargar();
	        this.alertService.success("Salida guardada");
	        this.loading = false;
	        this.modalRef?.hide();
	    },error => {
	        this.alertService.error(error.error);
	        this.loading = false;
	    });
	}

	onSubmitGasto() {
	    this.loading = true;
	    // Guardamos al gasto
	    this.apiService.store('gasto', this.gasto).subscribe(gasto => {
	        this.gasto = gasto;
	        this.cargar();
	        this.alertService.success("Gasto guardada");
	        this.loading = false;
	        this.modalRef?.hide();
	    },error => {
	        this.alertService.error(error.error);
	        this.loading = false;
	    });
	}

	onSubmitNota() {
	    this.loading = true;
	    // Guardamos al gasto
	    this.apiService.store('nota', this.nota).subscribe(nota => {
	        this.nota = nota;
	        this.cargar();
	        this.alertService.success("Nota guardada");
	        this.loading = false;
	        this.modalRef?.hide();
	    },error => {
	        this.alertService.error(error.error);
	        this.loading = false;
	    });
	}

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-entrada',
  templateUrl: './entrada.component.html'
})
export class EntradaComponent implements OnInit {

	public entrada: any = {};
    public loading = false;

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router
	) { }

	ngOnInit() {
	    
	    this.route.params.subscribe(params => {
	        
	        if(isNaN(params['id'])){
	            this.entrada = {};
	            this.entrada.fecha = this.apiService.date();
	            this.entrada.usuario_id = this.apiService.auth_user().id;
	            this.entrada.empresa_id = this.apiService.auth_user().empresa_id;
	        }
	        else{
	            // Optenemos el entrada
	            this.apiService.read('entrada/', params['id']).subscribe(entrada => {
	               this.entrada = entrada;
	            });
	        }

	    });

	}

	onSubmit() {
	    this.loading = true;
	    // Guardamos al entrada
	    this.apiService.store('entrada', this.entrada).subscribe(entrada => {
	        this.entrada = entrada;
	        console.log(entrada);
	        this.alertService.success("Gasto guardada");
	        this.loading = false;
	        this.router.navigate(['/entradas']);
	    },error => {
	        this.alertService.error(error);
	        this.loading = false;
	    });
	}

}

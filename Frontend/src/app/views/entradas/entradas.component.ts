import { Component, OnInit } from '@angular/core';

import { AlertService } from '../../services/alert.service';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-entradas',
  templateUrl: './entradas.component.html'
})
export class EntradasComponent implements OnInit {

	public entradas:any = [];
    public txt:any = '';
    public loading:boolean = false;

    constructor(private apiService: ApiService, private alertService: AlertService){ }

	ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.apiService.getAll('entradas').subscribe(entradas => { 
            this.entradas = entradas;
        }, error => {this.alertService.error(error); });
    }

    public search($text:any){
        if($text.length > 1) {
            this.apiService.read('entradas/buscar/', $text).subscribe(entradas => { 
                this.entradas = entradas;
            }, error => {this.alertService.error(error); });
        }
    }

    public delete($id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('gasto/', $id) .subscribe(data => {
                 for (let i = 0; i < this.entradas['data'].length; i++) { 
                    if (this.entradas['data'][i].id == data.id )
                        this.entradas['data'].splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }

    }

    public setPaginacion(page:any):void{
        this.loading = true;
        this.apiService.paginate(this.entradas.path + '?page='+ page).subscribe(entradas => { 
            this.entradas = entradas;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

}

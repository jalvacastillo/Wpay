import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-compras',
  templateUrl: './compras.component.html',
  styleUrls: ['./compras.component.css']
})

export class ClienteComprasComponent implements OnInit {

	public cliente: any = {};

    constructor(private apiService: ApiService, private alertService: AlertService, private route: ActivatedRoute, private router: Router){ }

	ngOnInit() {
		this.route.params.subscribe(params => {
	        this.apiService.read('cliente/ventas/', params['id']).subscribe(cliente => { 
	            this.cliente = cliente;
	            console.log(cliente);
	        }, error => {this.alertService.error(error); });
	    });
    }

    private search($text:any){
    	// if($text) {
	    // 	this.apiService.read('ventas/buscar/', $text).subscribe(ventas => { 
	    // 	    this.ventas = ventas;
	    // 	    this.paginacion = [];
	    // 	    for (let i = 0; i < ventas.last_page; i++) { this.paginacion.push(i+1); }
	    // 	}, error => {this.alertService.error(error); });
    	// }
    }

    // private delete($id) {
    //     if (confirm('¿Desea eliminar el Registro?')) {
    //         this.apiService.delete('venta/', $id) .subscribe(data => {
    //             for (let i in this.ventas['data']) {
    //                 if (this.ventas['data'][i].id == data.id )
    //                     this.ventas['data'].splice(i, 1);
    //             }
    //         }, error => {this.alertService.error(error); });
                   
    //     }

    // }

}

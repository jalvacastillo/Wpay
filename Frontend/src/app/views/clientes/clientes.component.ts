import { Component, OnInit } from '@angular/core';

import { AlertService } from '../../services/alert.service';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

	public clientes:any = [];
    public paginacion:any = [];
    public txt:any = '';
    public loading:boolean = false;

    constructor(private apiService: ApiService, private alertService: AlertService){ }

	ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        this.apiService.getAll('clientes').subscribe(clientes => { 
            this.clientes = clientes;
            this.paginacion = [];
            this.loading = false;
            for (let i = 0; i < clientes.last_page; i++) { this.paginacion.push(i+1); }
        }, error => {this.alertService.error(error); this.loading = false;});
    }

    public search($text:any){
    	if($text) {
	    	this.apiService.read('clientes/buscar/', $text).subscribe(clientes => { 
	    	    this.clientes = clientes;
	    	    this.paginacion = [];
	    	    for (let i = 0; i < clientes.last_page; i++) { this.paginacion.push(i+1); }
	    	}, error => {this.alertService.error(error); });
    	}
    }

    public delete($id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('cliente/', $id) .subscribe(data => {
                for (let i = 0; i < this.clientes['data'].length; i++) { 
                    if (this.clientes['data'][i].id == data.id )
                        this.clientes['data'].splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }

    }

    public setPaginacion(page:any):void{
        this.loading = true;
        this.apiService.paginate(this.clientes.path + '?page='+ page).subscribe(clientes => { 
            this.clientes = clientes;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }


}

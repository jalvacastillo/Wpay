import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-nota',
  templateUrl: './nota.component.html'
})
export class NotaComponent implements OnInit {

	public nota: any = {};
    public loading = false;

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router
	) { }

	ngOnInit() {
	    
	    this.route.params.subscribe(params => {
	        
	        if(isNaN(params['id'])){
	            this.nota = {};
	            this.nota.fecha = this.apiService.date();
	            this.nota.estado = 'Pendiente';
	            this.nota.usuario_id = this.apiService.auth_user().id;
	            this.nota.empresa_id = this.apiService.auth_user().empresa_id;
	        }
	        else{
	            // Optenemos el nota
	            this.apiService.read('nota/', params['id']).subscribe(nota => {
	               this.nota = nota;
	            });
	        }

	    });

	}

	onSubmit() {
	    this.loading = true;
	    // Guardamos al nota
	    this.apiService.store('nota', this.nota).subscribe(nota => {
	        this.nota = nota;
	        console.log(nota);
	        this.alertService.success("Nota guardada");
	        this.loading = false;
	        this.router.navigate(['/nota/'+ this.nota.id]);
	    },error => {
	        this.alertService.error(error._body);
	        this.loading = false;
	    });
	}

}

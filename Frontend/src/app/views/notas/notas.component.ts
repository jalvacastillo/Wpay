import { Component, OnInit } from '@angular/core';

import { AlertService } from '../../services/alert.service';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-notas',
  templateUrl: './notas.component.html'
})
export class NotasComponent implements OnInit {

	public notas:any = [];
    public paginacion:any = [];
    public txt:any = '';
    public loading:boolean = false;

    constructor(private apiService: ApiService, private alertService: AlertService){ }

	ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.apiService.getAll('notas').subscribe(notas => { 
            this.notas = notas;
            this.paginacion = [];
            for (let i = 0; i < notas.last_page; i++) { this.paginacion.push(i+1); }
        }, error => {this.alertService.error(error); });
    }

    public search($text:any){
        if($text.length > 1) {
            this.apiService.read('notas/buscar/', $text).subscribe(notas => { 
                this.notas = notas;
                this.paginacion = [];
                for (let i = 0; i < notas.last_page; i++) { this.paginacion.push(i+1); }
            }, error => {this.alertService.error(error); });
        }
    }

    public delete($id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('nota/', $id) .subscribe(data => {
                for (let i = 0; i < this.notas['data'].length; i++) { 
                    if (this.notas['data'][i].id == data.id )
                        this.notas['data'].splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }

    }

    public setEstado(nota:any, estado:string){
        nota.estado = estado;
        this.apiService.store('nota', nota).subscribe(nota => { 
            this.alertService.success('Actualizada');
        }, error => {this.alertService.error(error); });
    }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.notas.path + '?page='+ event.page).subscribe(notas => { 
            this.notas = notas;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SumPipe }     from '../../../services/sum.pipe';
import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-venta-factura',
  templateUrl: './venta-factura.component.html',
  styleUrls: ['./venta-factura.component.css'],
  providers: [ SumPipe ]
})

export class VentaFacturaComponent implements OnInit {

		public venta: any = {};
		public detalles: any[] = [];
		public detalle: any = {};
		
		public cliente: any = {};
		public clientes:any = [];
		public productos:any = [];
		public producto: any = {};
		public nuevo:any = {};
	    public loading = false;

		constructor( 
		    private apiService: ApiService, private alertService: AlertService,
		    private route: ActivatedRoute, private router: Router, private sumPipe:SumPipe,
		) { }

		ngOnInit() {
		    
		    this.route.params.subscribe(params => {
		        
		        if(isNaN(params['id'])){

		            this.venta.fecha = this.apiService.date();
		            this.venta.tipo = 'Consumidor Final';
		            this.venta.estado = 'Finalizado';
		            this.venta.usuario_id = this.apiService.auth_user().id;
		            this.venta.empresa_id = this.apiService.auth_user().empresa_id;

		        }
		        else{
		            // Optenemos el venta

		            this.apiService.read('venta/detalles/', params['id']).subscribe(venta => {
		                this.venta = venta;
		               	this.detalles = venta.detalles;

		               this.apiService.read('cliente/', venta.cliente_id).subscribe(cliente => {
		                  this.cliente = cliente;
		               });
		            });

		        }

		        this.detalle.cantidad = 1;

		    });

		}

		searchCliente(txt:any){
			if(txt) {
				this.apiService.read('clientes/buscar/', txt).subscribe(clientes => {
				   this.clientes = clientes;
				});
			}else{
				this.clientes.data = [];
				this.cliente = {};
			}
		}


		searchProducto(){
			if(this.detalle.descripcion) {
				this.apiService.read('productos/buscar/', this.detalle.descripcion).subscribe(productos => {
				   this.productos = productos;
				});
			}else{
				this.productos.data = [];
				this.producto = {};
			}
		}

		selectProducto(producto:any){
			this.producto = producto;
			this.detalle.descripcion = producto.nombre;
			this.detalle.precio = producto.precio;
			this.detalle.total = this.detalle.cantidad * this.detalle.precio;
			this.productos.data = [];
		}

		updateDetalle(detalle:any){
			this.detalle.total = this.detalle.cantidad * this.detalle.precio;
			this.sumTotal();
		}

		selectCliente(cliente:any){
			this.cliente = cliente;
			this.clientes.data = [];
		}

		selectDetalle(detalle:any){
			this.detalle = detalle;
		}

		public sumTotal() {
		    this.venta.iva_retenido = this.venta.retencion ? ((this.sumPipe.transform(this.detalles, 'total') / 1.13) * 0.01) : 0;
			this.venta.total = (parseFloat(this.sumPipe.transform(this.detalles, 'total')) - parseFloat(this.venta.iva_retenido)).toFixed(2);
		    console.log (this.venta);
		}

		onSubmit() {
			this.loading = true;

		    // Si no existe el cliente lo creamos
	    	this.cliente.empresa_id = this.apiService.auth_user().empresa_id;

	    	this.apiService.store('cliente', this.cliente).subscribe(cliente => {
	        	this.cliente = cliente;
        	    this.venta.cliente_id = this.cliente.id;
        	    
        	    this.apiService.store('venta', this.venta).subscribe(venta => {
        	        this.venta = venta;
        	        this.loading = false;
	        		this.router.navigate(['venta/factura/', venta.id]);
        		}, error => {this.alertService.error(error); });

	    	}, error => {this.alertService.error(error); });

		}

		agregarDetalle(detalle:any){
			console.log(detalle);
	        detalle.venta_id = this.venta.id;
	        detalle.pago_pendiente = 0;
            this.apiService.store('venta/detalle', detalle).subscribe(detalle => {
                this.apiService.read('venta/detalles/', this.venta.id).subscribe(venta => {
                    this.venta = venta;
                   	this.detalles = venta.detalles;
                });
                this.detalle = {};
                this.detalle.cantidad = 1;
        	}, error => {this.alertService.error(error); });
		}

		eliminarDetalle(detalle:any){
			if (confirm('¿Desea eliminar el Registro?')) {
				this.apiService.delete('venta/detalle/', detalle.id).subscribe(detalle => {
					for (var i = 0; i < this.detalles.length; ++i) {
						if (this.detalles[i].id === detalle.id ){
							this.detalles.splice(i, 1);
						}
					}
					this.apiService.read('venta/detalles/', this.venta.id).subscribe(venta => {
					    this.venta = venta;
					   	this.detalles = venta.detalles;
					});
	        	}, error => {this.alertService.error(error); });
			}
		}

}



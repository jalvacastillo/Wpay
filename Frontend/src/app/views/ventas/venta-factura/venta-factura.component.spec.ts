import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VentaFacturaComponent } from './venta-factura.component';

describe('VentaFacturaComponent', () => {
  let component: VentaFacturaComponent;
  let fixture: ComponentFixture<VentaFacturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VentaFacturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VentaFacturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AlertService } from '../../services/alert.service';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent implements OnInit {

		public ventas:any = {};
	    public paginacion:any = [];
	    public usuarios:any = [];
	    public clientes:any = [];
	    public txt:any = '';
	    public filtro:any = {};
	    public filtrado:boolean = false;
        public loading:boolean = false;

	    modalRef?: BsModalRef;

	    constructor(
	        public apiService: ApiService, private alertService: AlertService, 
	        private modalService: BsModalService
	    ){}

		ngOnInit() {
	        this.loadAll();
	    }

	    public loadAll() {
	        this.apiService.getAll('ventas').subscribe(ventas => { 
	            this.ventas = ventas;
	            this.paginacion = [];
	            for (let i = 0; i < ventas.last_page; i++) { this.paginacion.push(i + 1); }
	        }, error => {this.alertService.error(error); });
	    }

	    public search($text:any){
	    	if($text) {
		    	this.apiService.read('ventas/buscar/', $text).subscribe(ventas => { 
		    	    this.ventas = ventas;
		    	    this.paginacion = [];
		    	    for (let i = 0; i < ventas.last_page; i++) { this.paginacion.push(i + 1); }
		    	}, error => {this.alertService.error(error); });
	    	}
	    }

	    public delete($id:number) {
	        if (confirm('¿Desea eliminar el Registro?')) {
	            this.apiService.delete('venta/', $id) .subscribe(data => {
	                for (let i = 0; i < this.ventas['data'].length; i++) { 
                       if (this.ventas['data'][i].id == data.id )
                           this.ventas['data'].splice(i, 1);
                   }
	            }, error => {this.alertService.error(error); });
	                   
	        }

	    }

	    public setEstado(venta:any, estado:string){
	        venta.estado = estado;
	        this.apiService.store('venta', venta).subscribe(venta => { 
	            this.alertService.success('Actualizado');
	        }, error => {this.alertService.error(error); });
	    }

	    public setPaginacion(page:any):void{
            console.log(page);
            this.loading = true;
            this.apiService.paginate(this.ventas.path + '?page='+ page).subscribe(ventas => { 
                this.ventas = ventas;
                this.loading = false;
            }, error => {this.alertService.error(error); this.loading = false;});
        }


        // Filtros
        openFilter(template: TemplateRef<any>) {     

            if(!this.filtrado) {
                this.filtro.inicio = null;
                this.filtro.fin = null;
                this.filtro.usuario_id = '';
                this.filtro.cliente_id = '';
                this.filtro.estado = '';
                this.filtro.tipo = '';

                this.apiService.getAll('clientes/list').subscribe(clientes => { 
                    this.clientes = clientes;
                }, error => {this.alertService.error(error); });

                this.apiService.getAll('usuarios/list').subscribe(usuarios => { 
                    this.usuarios = usuarios;
                }, error => {this.alertService.error(error); });

            }

            this.modalRef = this.modalService.show(template);
        }

        onFiltrar(){
            this.loading = true;
            this.apiService.store('ventas/filtrar', this.filtro).subscribe(ventas => { 
                this.ventas = ventas;
                this.loading = false; this.filtrado = true;
                console.log(this.filtrado);
                this.modalRef!.hide();
            }, error => {this.alertService.error(error); this.loading = false;});

        }

}

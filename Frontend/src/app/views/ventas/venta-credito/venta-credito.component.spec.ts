import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VentaCreditoComponent } from './venta-credito.component';

describe('VentaCreditoComponent', () => {
  let component: VentaCreditoComponent;
  let fixture: ComponentFixture<VentaCreditoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VentaCreditoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VentaCreditoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

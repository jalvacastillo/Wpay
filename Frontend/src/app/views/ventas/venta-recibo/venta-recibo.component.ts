import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-venta-recibo',
  templateUrl: './venta-recibo.component.html',
  styleUrls: ['./venta-recibo.component.css']
})

export class VentaReciboComponent implements OnInit {

		public venta: any = {};
		public detalles: any[] = [];
		public detalle: any = {};
		
		public cliente: any = {};
		public clientes:any = [];
		public nuevo:any = {};
	    public loading = false;

		constructor( 
		    public apiService: ApiService, private alertService: AlertService,
		    private route: ActivatedRoute, private router: Router
		) { }

		ngOnInit() {
		    
		    this.route.params.subscribe(params => {
		        
		        if(isNaN(params['id'])){

		            this.venta.fecha = this.apiService.date();
		            this.venta.tipo = 'Recibo';
		            this.venta.estado = 'Orden';
		            this.venta.usuario_id = this.apiService.auth_user().id;
		            this.venta.empresa_id = this.apiService.auth_user().empresa_id;
		            
		            this.detalle.pago_total = false;
		            this.detalle.pago_pendiente = 0;
		        }
		        else{
		            // Optenemos el venta

		            this.apiService.read('venta/detalles/', params['id']).subscribe(venta => {
		               this.venta = venta;
		               	if(venta.detalles.length > 0) {
		               		this.detalle = venta.detalles[0];
		               	}
		               this.apiService.read('cliente/', venta.cliente_id).subscribe(cliente => {
		                  this.cliente = cliente;
		               });
		            });
		        }

		    });

		}

		searchCliente(txt:any){
			if(txt) {
				this.apiService.read('clientes/buscar/', txt).subscribe(clientes => {
				   this.clientes = clientes;
				});
			}else{
				this.clientes.data = [];
				this.cliente = {};
			}
		}

		setPago(){
			console.log('ser');
			this.detalle.pago_pendiente = this.detalle.precio - this.detalle.abono;
			this.detalle.total = this.detalle.precio;
		}

		selectCliente(cliente:any){
			this.cliente = cliente;
			this.clientes.data = [];
		}

		onSubmit() {
			this.loading = true;
			if(this.detalle.pago_total) {
				this.detalle.pago_pendiente = 0;
				this.detalle.abono = this.detalle.precio;
			}

	    	this.cliente.empresa_id = this.apiService.auth_user().empresa_id;

	    	this.apiService.store('cliente', this.cliente).subscribe(cliente => {
	        	this.cliente = cliente;

        	    this.venta.cliente_id = this.cliente.id;
        	    this.apiService.store('venta', this.venta).subscribe(venta => {
        	        this.venta = venta;
        	        this.detalle.venta_id = venta.id;
    	            this.apiService.store('venta/detalle', this.detalle).subscribe(detalle => {
    	                this.detalle = detalle;
    	                this.loading = false;
    	                this.alertService.success("Guardado");
    	        		this.router.navigate(['venta/recibo/', venta.id]);
    	        	}, error => {this.loading = false; this.alertService.error(error); });
        		}, error => {this.loading = false; this.alertService.error(error); });

	    	}, error => {this.loading = false; this.alertService.error(error); });
		    
		    

		}

}


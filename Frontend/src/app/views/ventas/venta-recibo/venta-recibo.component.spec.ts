import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VentaReciboComponent } from './venta-recibo.component';

describe('VentaReciboComponent', () => {
  let component: VentaReciboComponent;
  let fixture: ComponentFixture<VentaReciboComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VentaReciboComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VentaReciboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

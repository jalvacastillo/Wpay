import { Component, OnInit } from '@angular/core';

import { AlertService } from '../../services/alert.service';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-compras',
  templateUrl: './compras.component.html',
  styleUrls: ['./compras.component.css']
})
export class ComprasComponent implements OnInit {

	public compras:any = [];
    public paginacion:any = [];
    public txt:any = '';
    public loading:boolean = false;

    constructor(private apiService: ApiService, private alertService: AlertService){ }

	ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.apiService.getAll('compras').subscribe(compras => { 
            this.compras = compras;
            this.paginacion = [];
            for (let i = 0; i < compras.last_page; i++) { this.paginacion.push(i+1); }
        }, error => {this.alertService.error(error); });
    }

    public search($text:any){
    	if($text.length > 1) {
	    	this.apiService.read('compras/buscar/', $text).subscribe(compras => { 
	    	    this.compras = compras;
	    	    this.paginacion = [];
	    	    for (let i = 0; i < compras.last_page; i++) { this.paginacion.push(i+1); }
	    	}, error => {this.alertService.error(error); });
    	}
    }

    public delete($id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('compra/', $id) .subscribe(data => {
                 for (let i = 0; i < this.compras['data'].length; i++) { 
                    if (this.compras['data'][i].id == data.id )
                        this.compras['data'].splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }

    }

    public setPaginacion(page:any):void{
        this.loading = true;
        this.apiService.paginate(this.compras.path + '?page='+ page).subscribe(compras => { 
            this.compras = compras;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-compra',
  templateUrl: './compra.component.html',
  styleUrls: ['./compra.component.css']
})
export class CompraComponent implements OnInit {

	public compra: any = {};
    public loading = false;

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router
	) { }

	ngOnInit() {
	    
	    this.route.params.subscribe(params => {
	        
	        if(isNaN(params['id'])){
	            this.compra = {};
	            this.compra.fecha = this.apiService.date();
	            this.compra.usuario_id = this.apiService.auth_user().id;
	            this.compra.empresa_id = this.apiService.auth_user().empresa_id;
	        }
	        else{
	            // Optenemos el compra
	            this.apiService.read('compra/', params['id']).subscribe(compra => {
	               this.compra = compra;
	            });
	        }

	    });

	}

	onSubmit() {
	    this.loading = true;
	    // Guardamos al compra
	    this.apiService.store('compra', this.compra).subscribe(compra => {
	        this.compra = compra;
	        console.log(compra);
	        this.alertService.success("Compra guardada");
	        this.loading = false;
	        this.router.navigate(['/compra/'+ this.compra.id]);
	    },error => {
	        this.alertService.error(error._body);
	        this.loading = false;
	    });
	}

}

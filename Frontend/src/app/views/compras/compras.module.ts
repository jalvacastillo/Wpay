import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ComprasComponent } from './compras.component';
import { CompraComponent } from './compra/compra.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  declarations: [
  	ComprasComponent,
    CompraComponent
  ],
  exports: [
  	ComprasComponent,
    CompraComponent
  ]
})
export class ComprasModule { }

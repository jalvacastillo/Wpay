import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-datos-precios',
  templateUrl: './datos-precios.component.html'
})
export class DatosPreciosComponent implements OnInit {

    public precios: any = {};
    public loading: boolean = false;

  	constructor( 
  	    private apiService: ApiService, private alertService: AlertService,
  	    private route: ActivatedRoute, private router: Router
  	) { }

  	ngOnInit() {
  	    
        this.apiService.read('datos-precio/', 1).subscribe(precios => {
            this.precios = precios;
        },error => {this.alertService.error(error); });


  	}

  
  	public onSubmit() {
  	    this.loading = true;

  	    this.apiService.store('datos-precio', this.precios).subscribe(precios => {
          this.precios = precios;
          this.loading = false;
        },error => {this.alertService.error(error._body); this.loading = false; });
  	}

}

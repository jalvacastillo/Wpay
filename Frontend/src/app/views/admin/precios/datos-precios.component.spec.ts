import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosPreciosComponent } from './datos-precios.component';

describe('DatosPreciosComponent', () => {
  let component: DatosPreciosComponent;
  let fixture: ComponentFixture<DatosPreciosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosPreciosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosPreciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-cajas',
  templateUrl: './cajas.component.html'
})
export class CajasComponent implements OnInit {

    public cajas: any[] = [];
    public cortes:any = [];
    public caja: any = {};
    public corte: any = {};
    public loading = false;

    modalRef?: BsModalRef;

  	constructor( 
  	    private apiService: ApiService, private alertService: AlertService,
  	    private route: ActivatedRoute, private router: Router,
        private modalService: BsModalService
  	) { }

  	ngOnInit() {
  	    
        this.apiService.getAll('cajas').subscribe(cajas => {
            this.cajas = cajas;
        },error => {this.alertService.error(error); });

        this.apiService.getAll('cortes').subscribe(cortes => {
            this.cortes = cortes;
        },error => {this.alertService.error(error); });


  	}

    openModal(template: TemplateRef<any>, caja:any) {
        this.caja = caja;
        this.modalRef = this.modalService.show(template);
    }

  	public onSubmit() {
  	    this.loading = true;
  	    // Guardamos la caja
        this.corte.caja_id = this.caja.id;
        this.corte.usuario_id = this.apiService.auth_user().id;
        this.corte.empresa_id = this.apiService.auth_user().empresa_id;
        this.caja.empresa_id = this.apiService.auth_user().empresa_id;

  	    this.apiService.store('caja', this.caja).subscribe(caja => {
            this.apiService.store('corte', this.corte).subscribe(corte => {
      	        this.caja.total = corte.total;
      	        this.alertService.success("Datos guardados");
      	        this.loading = false;
                this.modalRef?.hide();
      	    },error => {this.alertService.error(error._body); this.loading = false; });

        },error => {this.alertService.error(error._body); this.loading = false; });
  	}

}

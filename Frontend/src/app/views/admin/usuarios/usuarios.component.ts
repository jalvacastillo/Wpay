import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})

export class UsuariosComponent implements OnInit {

		public usuarios:any = [];
	    public usuario:any = {};
	    public loading:boolean = false;

	    modalRef?: BsModalRef;

	    constructor(private apiService: ApiService, private alertService: AlertService, 
	    	private modalService: BsModalService
	    ){ }

		ngOnInit() {
	        this.loadAll();
	    }

	    public loadAll() {
	        this.apiService.getAll('usuarios').subscribe(usuarios => { 
	            this.usuarios = usuarios;
	        }, error => {this.alertService.error(error); });
	    }

	      openModal(template: TemplateRef<any>, usuario:any) {
	          this.usuario = usuario;
	          this.modalRef = this.modalService.show(template);
	      }

	    	public onSubmit() {
	    	    this.loading = true;
	    	    // Guardamos la usuario

	    	    this.apiService.store('usuario', this.usuario).subscribe(usuario => {
	        	        this.usuario = usuario;
	        	        this.alertService.success("Datos guardados");
	        	        this.loading = false;
	                  this.modalRef?.hide();
	          },error => {this.alertService.error(error._body); this.loading = false; });
	    	}

}


import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {

  public usuario: any = {};
	public empresa: any = {};
	public loading = false;

  	constructor( 
  	    private apiService: ApiService, private alertService: AlertService,
  	    private route: ActivatedRoute, private router: Router
  	) { }

  	ngOnInit() {
  	    
  	    this.usuario = JSON.parse(sessionStorage.getItem('auth_user')!);
  	    console.log(this.usuario);
        this.apiService.read('empresa/', this.usuario.empresa_id).subscribe(empresa => {
            this.empresa = empresa;
            console.log(empresa);
            this.loading = false;
        },error => {this.alertService.error(error._body); this.loading = false; });

  	}

  	onSubmit() {
  	    this.loading = true;
  	    // Guardamos la empresa
  	    this.apiService.store('empresa', this.empresa).subscribe(empresa => {
  	        this.empresa = empresa;
  	        console.log(empresa);
  	        this.alertService.success("Usuario guardado");
  	        this.loading = false;
  	    },error => {this.alertService.error(error._body); this.loading = false; });
  	}

}

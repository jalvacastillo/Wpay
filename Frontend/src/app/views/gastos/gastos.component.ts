import { Component, OnInit } from '@angular/core';

import { AlertService } from '../../services/alert.service';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-gastos',
  templateUrl: './gastos.component.html'
})
export class GastosComponent implements OnInit {

	public gastos:any = [];
    public paginacion:any = [];
    public txt:any = '';
    public loading:boolean = false;

    constructor(private apiService: ApiService, private alertService: AlertService){ }

	ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.apiService.getAll('gastos').subscribe(gastos => { 
            this.gastos = gastos;
            this.paginacion = [];
            for (let i = 0; i < gastos.last_page; i++) { this.paginacion.push(i+1); }
        }, error => {this.alertService.error(error); });
    }

    public search($text:any){
        if($text.length > 1) {
            this.apiService.read('gastos/buscar/', $text).subscribe(gastos => { 
                this.gastos = gastos;
                this.paginacion = [];
                for (let i = 0; i < gastos.last_page; i++) { this.paginacion.push(i+1); }
            }, error => {this.alertService.error(error); });
        }
    }

    public delete($id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('gasto/', $id) .subscribe(data => {
                 for (let i = 0; i < this.gastos['data'].length; i++) { 
                    if (this.gastos['data'][i].id == data.id )
                        this.gastos['data'].splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }

    }

    public setPaginacion(page:any):void{
        this.loading = true;
        this.apiService.paginate(this.gastos.path + '?page='+ page).subscribe(gastos => { 
            this.gastos = gastos;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

}

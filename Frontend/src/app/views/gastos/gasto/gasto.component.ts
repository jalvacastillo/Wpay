import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-gasto',
  templateUrl: './gasto.component.html'
})
export class GastoComponent implements OnInit {

	public gasto: any = {};
    public loading = false;

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router
	) { }

	ngOnInit() {
	    
	    this.route.params.subscribe(params => {
	        
	        if(isNaN(params['id'])){
	            this.gasto = {};
	            this.gasto.fecha = this.apiService.date();
	            this.gasto.usuario_id = this.apiService.auth_user().id;
	            this.gasto.empresa_id = this.apiService.auth_user().empresa_id;
	        }
	        else{
	            // Optenemos el gasto
	            this.apiService.read('gasto/', params['id']).subscribe(gasto => {
	               this.gasto = gasto;
	            });
	        }

	    });

	}

	onSubmit() {
	    this.loading = true;
	    // Guardamos al gasto
	    this.apiService.store('gasto', this.gasto).subscribe(gasto => {
	        this.gasto = gasto;
	        console.log(gasto);
	        this.alertService.success("Gasto guardada");
	        this.loading = false;
	        this.router.navigate(['/gastos']);
	    },error => {
	        this.alertService.error(error);
	        this.loading = false;
	    });
	}

}

import { Component, OnInit } from '@angular/core';

import { AlertService } from '../../services/alert.service';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

	public productos:any = [];
    public paginacion:any = [];
    public txt:any = '';
    public loading:boolean = false;

    constructor(private apiService: ApiService, private alertService: AlertService){ }

	ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.apiService.getAll('productos').subscribe(productos => { 
            this.productos = productos;
            this.paginacion = [];
            for (let i = 0; i < productos.last_page; i++) { this.paginacion.push(i+1); }
        }, error => {this.alertService.error(error); });
    }

    public search($text:any){
    	if($text) {
	    	this.apiService.read('productos/buscar/', $text).subscribe(productos => { 
	    	    this.productos = productos;
	    	    this.paginacion = [];
	    	    for (let i = 0; i < productos.last_page; i++) { this.paginacion.push(i+1); }
	    	}, error => {this.alertService.error(error); });
    	}
    }

    public delete($id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('cliente/', $id) .subscribe(data => {
                for (let i = 0; i < this.productos['data'].length; i++) { 
                    if (this.productos['data'][i].id == data.id )
                        this.productos['data'].splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }

    }

    public setPaginacion(page:any):void{
        this.loading = true;
        this.apiService.paginate(this.productos.path + '?page='+ page).subscribe(productos => { 
            this.productos = productos;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }


}

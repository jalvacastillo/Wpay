import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

	public producto: any = {};
    public loading = false;

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router
	) { }

	ngOnInit() {
	    
	    this.route.params.subscribe(params => {
	        
	        if(isNaN(params['id'])){
	            this.producto = {};
	            this.producto.usuario_id = this.apiService.auth_user().id;
		        this.producto.empresa_id = this.apiService.auth_user().empresa_id;
	        }
	        else{
	            // Optenemos el producto
	            this.apiService.read('producto/', params['id']).subscribe(producto => {
	               this.producto = producto;
	            });
	        }

	    });

	}

	onSubmit() {
	    this.loading = true;
	    // Guardamos al producto
	    this.apiService.store('producto', this.producto).subscribe(producto => {
	        this.producto = producto;
	        console.log(producto);
	        this.alertService.success("Cliente guardado");
	        this.loading = false;
	        this.router.navigate(['/producto/'+ this.producto.id]);
	    },error => {
	        this.alertService.error(error._body);
	        this.loading = false;
	    });
	}

}

import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { PerfilComponent } from './views/admin/perfil/perfil.component';

// Admin
import { EmpresaComponent } from './views/admin/empresa/empresa.component';
import { UsuariosComponent } from './views/admin/usuarios/usuarios.component';

import { DashComponent } from './dash/dash.component';

import { ClientesComponent } from './views/clientes/clientes.component';
import { ClienteComponent } from './views/clientes/cliente/cliente.component';
import { ClienteComprasComponent } from './views/clientes/compras/compras.component';

import { ProductosComponent } from './views/productos/productos.component';
import { ProductoComponent } from './views/productos/producto/producto.component';

import { ComprasComponent } from './views/compras/compras.component';
import { CompraComponent } from './views/compras/compra/compra.component';
import { GastosComponent } from './views/gastos/gastos.component';
import { GastoComponent } from './views/gastos/gasto/gasto.component';
import { EntradasComponent } from './views/entradas/entradas.component';
import { EntradaComponent } from './views/entradas/entrada/entrada.component';
import { NotasComponent } from './views/notas/notas.component';
import { NotaComponent } from './views/notas/nota/nota.component';

import { VentasComponent } from './views/ventas/ventas.component';
import { VentaReciboComponent } from './views/ventas/venta-recibo/venta-recibo.component';
import { VentaFacturaComponent } from './views/ventas/venta-factura/venta-factura.component';
import { VentaCreditoComponent } from './views/ventas/venta-credito/venta-credito.component';
import { PreciosComponent } from './views/precios/precios.component';

import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    
    { path: 'admin/perfil', component: PerfilComponent },
    { path: 'admin/empresa', component: EmpresaComponent },
    { path: 'admin/usuarios', component: UsuariosComponent },

    { path: 'dashboard', component: DashComponent, canActivate: [AuthGuard] },
    { path: 'clientes', component: ClientesComponent, canActivate: [AuthGuard] },

    { path: 'compras', component: ComprasComponent, canActivate: [AuthGuard] },
    { path: 'compra/:id', component: CompraComponent, canActivate: [AuthGuard] },
    { path: 'gastos', component: GastosComponent, canActivate: [AuthGuard] },
    { path: 'gasto/:id', component: GastoComponent, canActivate: [AuthGuard] },
    { path: 'entradas', component: EntradasComponent, canActivate: [AuthGuard] },
    { path: 'entrada/:id', component: EntradaComponent, canActivate: [AuthGuard] },
    { path: 'notas', component: NotasComponent, canActivate: [AuthGuard] },
    { path: 'nota/:id', component: NotaComponent, canActivate: [AuthGuard] },

    { path: 'productos', component: ProductosComponent, canActivate: [AuthGuard] },
    { path: 'producto/:id', component: ProductoComponent, canActivate: [AuthGuard] },

    { path: 'cliente/:id', component: ClienteComponent, canActivate: [AuthGuard] },
    { path: 'cliente/compras/:id', component: ClienteComprasComponent, canActivate: [AuthGuard] },
    { path: 'ventas', component: VentasComponent, canActivate: [AuthGuard] },
    { path: 'venta/recibo/:id', component: VentaReciboComponent, canActivate: [AuthGuard] },
    { path: 'venta/factura/:id', component: VentaFacturaComponent, canActivate: [AuthGuard] },
    { path: 'venta/credito/:id', component: VentaCreditoComponent, canActivate: [AuthGuard] },
    { path: 'precios', component: PreciosComponent, canActivate: [AuthGuard] },

    // otherwise redirect to home
    { path: '**', redirectTo: 'dashboard' }
];

export const routing = RouterModule.forRoot(appRoutes);

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
// import { Observable } from 'rxjs';
// import { Subject } from 'rxjs/Subject';

declare var $: any;

@Injectable()
export class AlertService {

    constructor(private router: Router) {}

    success(message: any) {
        console.log(message);
        $.notify({icon: "fa fa-check-circle fa-2x", message: message },{
            type: 'success',
            timer: 1000,
            placement: {from: 'bottom', align: 'center'}
        });

    }

    info(message: any) {
        console.log(message);
        $.notify({icon: "fa fa-info-circle fa-2x", message: message },{
            type: 'info',
            timer: 1000,
            placement: {from: 'bottom', align: 'center'}
        });

    }

    error(message: any) {
        console.log(message);
        if(message.status == 0) {
            $.notify({icon: "fa fa-exclamation-circle fa-2x", message: '<p>No hay conección al servidor</p>' },{
               type: 'danger',
               timer: 1000,
               placement: {from: 'bottom', align: 'center'}
            });

        }
        else if(message.status == 404) {
            $.notify({icon: "fa fa-exclamation-circle fa-2x", message: '<p>El registro no ha sido encontrado</p>' },{
               type: 'danger',
               timer: 1000,
               placement: {from: 'bottom', align: 'center'}
            });
        }
        else if(message.status == 401) {
            $.notify({icon: "fa fa-exclamation-circle fa-2x", message: message.error.error },{
               type: 'danger',
               timer: 1000,
               placement: {from: 'bottom', align: 'center'}
            });
            this.router.navigate(['/login']);
        }
        else if(message.status == 400) {
            $.notify({icon: "fa fa-exclamation-circle fa-2x", message: message.error.error },{
               type: 'danger',
               timer: 1000,
               placement: {from: 'bottom', align: 'center'}
            });
        }
        else if(message.status == 422) {
            for (var i = 0; i < message.error.error.length; ++i) {
                $.notify({icon: "fa fa-exclamation-circle fa-2x", message: message.error.error[i] },{
                   type: 'danger',
                   timer: 1000,
                   placement: {from: 'bottom', align: 'center'}
                });
            }
        }
        else if(message.status == 500) {
            $.notify({icon: "fa fa-exclamation-circle fa-2x", message: message.error.error },{
               type: 'danger',
               timer: 1000,
               placement: {from: 'bottom', align: 'center'}
            });
        }
        else {
            $.notify({icon: "fa fa-exclamation-circle fa-2x", message: message },{
               type: 'danger',
               timer: 1000,
               placement: {from: 'bottom', align: 'center'}
            });
        }
        
    }

}

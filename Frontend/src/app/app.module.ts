import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { JwtInterceptor } from './services/JwtInterceptor';

import { PipesModule } from './pipes/pipes.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PopoverModule } from 'ngx-bootstrap/popover';

import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';

import { AlertService } from './services/alert.service';
import { ApiService } from './services/api.service';

import { LoginComponent } from './login/login.component';
import { DashComponent } from './dash/dash.component';

import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { ClientesComponent } from './views/clientes/clientes.component';
import { ClienteComponent } from './views/clientes/cliente/cliente.component';
import { ClienteComprasComponent } from './views/clientes/compras/compras.component';

import { ComprasModule } from './views/compras/compras.module';
import { GastosComponent } from './views/gastos/gastos.component';
import { GastoComponent } from './views/gastos/gasto/gasto.component';
import { EntradasComponent } from './views/entradas/entradas.component';
import { EntradaComponent } from './views/entradas/entrada/entrada.component';

import { NotasComponent } from './views/notas/notas.component';
import { NotaComponent } from './views/notas/nota/nota.component';

import { ProductosComponent } from './views/productos/productos.component';
import { ProductoComponent } from './views/productos/producto/producto.component';

import { VentasComponent } from './views/ventas/ventas.component';
import { PerfilComponent } from './views/admin/perfil/perfil.component';
import { EmpresaComponent } from './views/admin/empresa/empresa.component';
import { CajasComponent } from './views/admin/cajas/cajas.component';
import { UsuariosComponent } from './views/admin/usuarios/usuarios.component';
import { VentaReciboComponent } from './views/ventas/venta-recibo/venta-recibo.component';
import { VentaFacturaComponent } from './views/ventas/venta-factura/venta-factura.component';
import { VentaCreditoComponent } from './views/ventas/venta-credito/venta-credito.component';
import { PreciosComponent } from './views/precios/precios.component';
import { DatosPreciosComponent } from './views/admin/precios/datos-precios.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashComponent,
    HeaderComponent,
    FooterComponent,
    ClientesComponent,
    ClienteComponent,
    PerfilComponent,
    EmpresaComponent,
    CajasComponent,
    UsuariosComponent,
    VentasComponent,
    ClienteComprasComponent,
    VentaReciboComponent,
    VentaFacturaComponent,
    VentaCreditoComponent,
    PreciosComponent,
    GastosComponent,
    GastoComponent,
    EntradasComponent,
    EntradaComponent,
    NotasComponent,
    NotaComponent,
    DatosPreciosComponent,
    ProductosComponent,
    ProductoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    routing,
    PipesModule,
    ComprasModule,
    ModalModule.forRoot(),
    PopoverModule.forRoot(),
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }, AuthGuard, AdminGuard, AlertService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotasTable extends Migration {

	public function up()
	{
		Schema::create('notas', function(Blueprint $table)
		{
			$table->increments('id');

			$table->date('fecha');
			$table->text('descripcion')->nullable();
			$table->enum('estado', ['Pendiente', 'Finalizado']);
			$table->integer('usuario_id');
			$table->integer('empresa_id');

			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('notas');
	}

}

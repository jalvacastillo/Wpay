,<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasTable extends Migration
{

    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('codigo')->nullable();
            $table->date('fecha');
            $table->string('correlativo')->nullable();
            $table->decimal('iva_retenido', 9,2)->default(0);
            $table->decimal('iva', 9,2)->default(0);
            $table->decimal('subtotal', 9,2)->default(0);
            $table->decimal('total', 9,2)->default(0);
            $table->integer('cliente_id');
            $table->enum('estado', ['Orden', 'En Proceso', 'Finalizado', 'Anulada']);
            $table->enum('tipo', ['Recibo', 'Consumidor Final', 'Credito Fiscal']);
            $table->integer('usuario_id');
            $table->integer('empresa_id')->default(1);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCortesTable extends Migration {

	public function up()
	{
		Schema::create('cortes', function(Blueprint $table)
		{
			$table->increments('id');

			$table->decimal('total', 15, 2)->default(0);
			$table->datetime('inicio');
			$table->datetime('final')->nullable();
			$table->enum('estado', ['Abierta', 'Cerrada']);
			$table->integer('caja_id');
			$table->integer('usuario_id');

			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('cortes');
	}

}

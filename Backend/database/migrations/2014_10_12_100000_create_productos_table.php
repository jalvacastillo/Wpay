<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration {

    public function up()
    {
        Schema::create('productos', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre');
            $table->integer('stock')->default(0);
            $table->decimal('precio', 7, 2)->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('productos');
    }

}

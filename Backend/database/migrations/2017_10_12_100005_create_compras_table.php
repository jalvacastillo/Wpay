<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprasTable extends Migration {

	public function up()
	{
		Schema::create('compras', function(Blueprint $table)
		{
			$table->increments('id');

			$table->date('fecha');
			$table->string('proveedor')->nullable();
			$table->text('descripcion')->nullable();
			$table->string('nota')->nullable();
			$table->decimal('total', 7, 2)->nullable();
			$table->integer('usuario_id');
			$table->integer('empresa_id');

			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('compras');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCajasTable extends Migration {

	public function up()
	{
		Schema::create('cajas', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('nombre');
			$table->string('correlativo_credito')->default(0);
			$table->string('correlativo_factura')->default(0);
			$table->integer('empresa_id');

			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('cajas');
	}

}

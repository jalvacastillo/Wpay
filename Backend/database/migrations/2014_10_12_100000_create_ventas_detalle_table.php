<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasDetalleTable extends Migration
{

    public function up()
    {
        Schema::create('ventas_detalle', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('venta_id');
            $table->integer('cantidad')->default(1);
            $table->text('descripcion')->nullable();
            $table->decimal('precio')->nullable();
            $table->decimal('abono')->nullable();
            $table->decimal('total')->nullable();
            $table->decimal('pago_pendiente')->nullable();
            $table->boolean('pago_total')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ventas_detalle');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosPreciosTable extends Migration {

	public function up()
	{
		Schema::create('datos_precios', function(Blueprint $table)
		{
			$table->increments('id');

			$table->decimal('lona', 8,2);
			$table->decimal('vinil', 8,2);
			$table->decimal('diseno', 8,2);
			$table->decimal('troquelado', 8,2);
			$table->decimal('tornillos', 8,2);
			$table->decimal('pega', 8,2);
			$table->decimal('silicon', 8,2);
			$table->decimal('laminador', 8,2);

			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('datos_precios');
	}

}

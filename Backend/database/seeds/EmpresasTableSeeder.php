<?php

use Illuminate\Database\Seeder;
use App\Models\Empresa;
use App\Models\Producto;

class EmpresasTableSeeder extends Seeder
{

    public function run()
    {


        // $table = new Empresa;
        // $table->nombre          = 'Global Grafic';
        // $table->email           = 'info@websis.me';
        // $table->telefono        = '(+503)7891-2933';
        // $table->direccion       = 'Ilobasco, Cabañas, El Salvador';
        
        // $table->save();



        Producto::create(['nombre' => 'Gorra Sublimada de Mall', 'precio' => 5]);
        Producto::create(['nombre' => 'Taza Sublimada Blanc', 'precio' => 5]);
        Producto::create(['nombre' => 'Taza Sublimada de Color / Perlad', 'precio' => 6]);
        Producto::create(['nombre' => 'Taza Magica Sublimada', 'precio' => 7.5]);
        Producto::create(['nombre' => 'Camisa Blanca Sublimada Frente', 'precio' => 8]);
        Producto::create(['nombre' => 'Camisa Blanca Sublimada Frente y Atrá', 'precio' => 10]);
        Producto::create(['nombre' => 'Camisa Algodón con Vinil Textil solo Frente', 'precio' => 8]);
        Producto::create(['nombre' => 'Camisa Algodón con Vinil Textil Frente y Atrá', 'precio' => 10]);
        Producto::create(['nombre' => 'Carnet de Identificación solo Frente', 'precio' => 3.25]);
        Producto::create(['nombre' => 'Carnet de Identificación solo Frente y Atrá', 'precio' => 4.25]);
        Producto::create(['nombre' => 'Correa Negra para Carnet', 'precio' => 0.5]);
        Producto::create(['nombre' => 'Funda para Carnet', 'precio' => 0.5]);
        Producto::create(['nombre' => 'Laminador por metro', 'precio' => 4]);
        Producto::create(['nombre' => 'Ojetes', 'precio' => 0.25]);
        Producto::create(['nombre' => 'Laminado de Tabloide 12x18 cada lado', 'precio' => 1]);
        Producto::create(['nombre' => 'Pines Publicitarios', 'precio' => 1]);
        Producto::create(['nombre' => 'Gorra con DT', 'precio' => 7]);
        Producto::create(['nombre' => 'Sombrero Pequeño con DT', 'precio' => 7]);
        Producto::create(['nombre' => 'Sombrero Vaquero con DT', 'precio' => 10]);
        Producto::create(['nombre' => 'Solo Impresión en UVDTF carta', 'precio' => 5]);
        Producto::create(['nombre' => 'Cable UTP x metro', 'precio' => 0.5]);
        Producto::create(['nombre' => 'Video Balum Verde', 'precio' => 0.75]);
        Producto::create(['nombre' => 'Balum de Corriente Macho- Verd', 'precio' => 0.65]);
        Producto::create(['nombre' => 'Balum de Corriente Hembra- Verd', 'precio' => 0.65]);
        Producto::create(['nombre' => 'Fuente 12v 2amp', 'precio' => 8.5]);
        Producto::create(['nombre' => 'Conectores RJ45 para Re', 'precio' => 0.65]);
        Producto::create(['nombre' => 'Sobres Membretados', 'precio' => 0.1]);
        Producto::create(['nombre' => 'Hojas Membretadas', 'precio' => 0.05]);
        Producto::create(['nombre' => 'Fotocopias Negro Carta u Oficio', 'precio' => 0.05]);
        Producto::create(['nombre' => 'Fotocopias Color Carta u Oficio', 'precio' => 0.5]);
        Producto::create(['nombre' => 'Impresiones Negra Carta u Oficio', 'precio' => 0.05]);
        Producto::create(['nombre' => 'Impresiones Color Carta u Oficio', 'precio' => 0.6]);
        Producto::create(['nombre' => 'Chapeton Grande Unidad', 'precio' => 3]);
        Producto::create(['nombre' => 'Chapeton Mediano Unida', 'precio' => 2.5]);
        Producto::create(['nombre' => 'Chapeton Pequeño Unida', 'precio' => 2]);
        Producto::create(['nombre' => 'Laminador Duro de Documentos', 'precio' => 1]);
        Producto::create(['nombre' => 'Laminador Duro Media Cart', 'precio' => 2]);
        Producto::create(['nombre' => 'Laminador Duro Carta', 'precio' => 2.5]);
        Producto::create(['nombre' => 'Laminador Duro Oficio', 'precio' => 2.75]);
        Producto::create(['nombre' => 'Hidrogel', 'precio' => 5]);
        Producto::create(['nombre' => 'Pulseras para Eventos SIN impresió', 'precio' => 0.1]);
        Producto::create(['nombre' => 'Pulseras para Eventos CON impresió', 'precio' => 0.15]);
        Producto::create(['nombre' => 'Yarda de Lona sin Impresión Ancho 1 mt', 'precio' => 3.5]);
        Producto::create(['nombre' => 'Yarda de Lona sin Impresión Ancho 1.35 mt', 'precio' => 3.75]);
        Producto::create(['nombre' => 'Yarda de Vinil sin impresion ancho 1 mt', 'precio' => 3.5]);
        Producto::create(['nombre' => 'Yarda de Vinil sin impresion ancho 1.3', 'precio' => 4]);
        Producto::create(['nombre' => 'Yarda de Vinil Colores Sólidos ancho 60cm sin cort', 'precio' => 2.8]);
        Producto::create(['nombre' => 'Llaveros Acrílico', 'precio' => 0.35]);
        Producto::create(['nombre' => 'Araña 0.60x1.60mtr con impresió', 'precio' => 20]);
        Producto::create(['nombre' => 'Araña 0.80x1.80mtr con Impresió', 'precio' => 28]);
        Producto::create(['nombre' => 'Rollup 0.80x2Mtr con Impresió', 'precio' => 35]);
        Producto::create(['nombre' => 'Diplomas tamaño carta', 'precio' => 1]);
        Producto::create(['nombre' => 'Tarjetas de Presentación ciento solo frent', 'precio' => 7.5]);
        Producto::create(['nombre' => 'Tarjetas de Presentación ciento ambos lados', 'precio' => 12]);
        Producto::create(['nombre' => 'Escarapela sublimada', 'precio' => 1]);


            
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\VentaDetalle;

class VentasDetalleTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();

            for($i = 1; $i <= 20 ; $i++)
            {
                $table = new VentaDetalle;
                $table->cantidad        = $faker->numberBetween(1,20);
                $table->descripcion     ='Lorem ipsum dolor sit amet, consectetur.';
                $table->precio          = $faker->numberBetween(1,20);
                $table->pago_pendiente  = $faker->numberBetween(1,20);
                $table->pago_total      = $faker->numberBetween(0,1);
                $table->abono           = $faker->numberBetween(0,1);
                $table->venta_id        = $faker->numberBetween(0,20);
                
                $table->save();

            }
            
    }
}

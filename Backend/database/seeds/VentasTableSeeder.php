<?php

use Illuminate\Database\Seeder;
use App\Models\Venta;

class VentasTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();

            for($i = 1; $i <= 20 ; $i++)
            {
                $table = new Venta;
                $table->codigo       = $i;
                $table->fecha       = $faker->date;
                $table->cliente_id  = $faker->numberBetween(1,20);
                $table->estado      = $faker->numberBetween(1,4);
                $table->tipo        = $faker->numberBetween(1,3);
                $table->tipo        = $faker->numberBetween(1,3);
                $table->total       = $faker->numberBetween(10,300);
                $table->iva         = $table->total * 0.13;
                $table->subtotal    = $table->total - $table->iva;
                $table->usuario_id  = 1;
                $table->empresa_id  = 1;
                
                $table->save();

            }
            
    }
}

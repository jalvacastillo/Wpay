<?php

use Illuminate\Database\Seeder;
use App\Models\Cliente;

class ClientesTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();

            $table = new Cliente;
            $table->nombre          = 'Sin Registrar';
            $table->empresa_id      = 1;
            $table->save();

            for($i = 1; $i <= 20 ; $i++)
            {
                $table = new Cliente;
                $table->nombre          = $faker->name;
                $table->nit             = '0906-020491-103-6';
                $table->nrc             = '1234567';
                $table->giro            = $faker->company;
                $table->telefono        = $faker->phoneNumber   ;
                $table->email           = $faker->email;
                $table->direccion       = $faker->address;
                $table->municipio       = $faker->word;
                $table->departamento    = $faker->word;
                $table->empresa_id      = 1;
                
                $table->save();

            }
            
    }
}

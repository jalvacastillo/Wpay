<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EmpresasTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        $this->call(ClientesTableSeeder::class);
        $this->call(VentasTableSeeder::class);
        $this->call(VentasDetalleTableSeeder::class);
        $this->call(VentasDetalleTableSeeder::class);
        $this->call(ComprasTableSeeder::class);
    }
}

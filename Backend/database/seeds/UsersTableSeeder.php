<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Caja;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

            $user = new User;
            $user->name = 'Global Grafic';
            $user->email = 'admin@admin.com';
            $user->tipo = 'Administrador';
            $user->password = Hash::make('admin');
            $user->empresa_id = 1;
            $user->caja_id = 1;
            $user->save();

            $user = new User;
            $user->name = 'Global Grafic';
            $user->email = 'emple@emple.com';
            $user->tipo = 'Empleado';
            $user->password = Hash::make('emple');
            $user->empresa_id = 1;
            $user->caja_id = 1;
            $user->save();

            $caja = new Caja;
            $caja->nombre = 'Caja 1';
            $caja->empresa_id = 1;
            $caja->save();
            
    }
}

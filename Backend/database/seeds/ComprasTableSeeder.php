<?php

use Illuminate\Database\Seeder;
use App\Models\Compra;
use Faker\Factory as Faker;
     
class ComprasTableSeeder extends Seeder {
     
    public function run()
    {
        $faker = Faker::create();

        for($i = 1; $i <= 30 ; $i++)
        {
            $table = new Compra;

            $table->fecha = $faker->date;
            $table->proveedor = $faker->name;
            $table->descripcion = "Lorem ipsum dolor sit.";
            $table->nota = "Lorem ipsum dolor sit.";
            $table->total = $faker->numberBetween(1,30);
            $table->usuario_id = 1;
            $table->empresa_id = 1;
            
            $table->save();
            
        }
    }
     
}
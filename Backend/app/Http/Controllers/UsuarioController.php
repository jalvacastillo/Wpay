<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Requests\UsuarioRequest;
use App\User as Usuario;

class UsuarioController extends Controller
{
    

    public function index() {
       
        $usuarios = Usuario::orderBy('id','dsc')->paginate(15);

        return Response()->json($usuarios, 200);

    }

    public function list() {

        $usuarios = Usuario::orderBy('name','dsc')->get();

        return Response()->json($usuarios, 200);            

    }

    public function read($id) {

        $usuario = Usuario::find($id);
        return Response()->json($usuario, 200);
            
    }

    public function search($txt) {

        $usuarios = Usuario::where('nombre', 'like' ,'%' . $txt . '%')->paginate(7);
        return Response()->json($usuarios, 200);

    }


    
    public function store(UsuarioRequest $request)
    {
        if($request->id){
            $usuario = Usuario::findOrFail($request->id);
        }
        else{
            $usuario = new Usuario;
        }


        if ($request->password) {
            $request['password'] = \Hash::make($request->password);
        }
        
        $usuario->fill($request->all());
        $usuario->save();

        return Response()->json($usuario, 200);


    }

    public function delete($id)
    {

        $usuario = Usuario::find($id);
        $usuario->delete();

        return Response()->json($usuario, 201);

    }

}

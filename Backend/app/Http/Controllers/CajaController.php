<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Requests\CajaRequest;
use App\Models\Caja;

class CajaController extends Controller
{
    

    public function index() {

        $cajas = Caja::orderBy('id','dsc')->get();

        return Response()->json($cajas, 200);            

    }


    public function read($id) {

        $caja = Caja::find($id);
        return Response()->json($caja, 200);

    }


    public function store(CajaRequest $request)
    {

        if($request->id){
            $caja = Caja::find($request->id);
        }
        else{
            $caja = new Caja;
        }
        
        $caja->fill($request->all());
        $caja->save();

        return Response()->json($caja, 200);

    }

    public function delete($id)
    {
        
        $caja = Caja::find($id);
        $caja->delete();

        return Response()->json($caja, 201);


    }

}

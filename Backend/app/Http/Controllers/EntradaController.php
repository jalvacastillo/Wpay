<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Requests\EntradaRequest;
use App\Models\Entrada;

class EntradaController extends Controller
{
    

    public function index() {

        $entradas = Entrada::orderBy('id','dsc')->paginate(15);

        return Response()->json($entradas, 200);            

    }


    public function read($id) {

        $entrada = Entrada::find($id);
        return Response()->json($entrada, 200);

    }


    public function store(EntradaRequest $request)
    {

        if($request->id){
            $entrada = Entrada::find($request->id);
        }
        else{
            $entrada = new Entrada;
        }
        
        $entrada->fill($request->all());
        $entrada->save();

        return Response()->json($entrada, 200);

    }

    public function delete($id)
    {
        
        $entrada = Entrada::find($id);
        $entrada->delete();

        return Response()->json($entrada, 201);


    }

}

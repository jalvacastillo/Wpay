<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Requests\CompraRequest;
use App\Models\Compra;

class ComprasController extends Controller
{
    

    public function index() {
       
        $lab_id = 1;

        $compras = Compra::where('empresa_id', $lab_id)->orderBy('id','dsc')->paginate(15);

        return Response()->json($compras, 200);
           

    }


    public function read($id) {

        $compra = Compra::findOrFail($id);
        return Response()->json($compra, 200);
 
    }


    public function search($txt) {

        $lab_id = 1;
        
        $compras = Compra::where('empresa_id', $lab_id)->where('nombre', 'like' ,'%' . $txt . '%')->paginate(7);
        return Response()->json($compras, 200);
            
    }


    public function store(CompraRequest $request)
    {
        if($request->id){
            $compra = Compra::findOrFail($request->id);
        }
        else{
            $compra = new Compra;
        }
        
        $compra->fill($request->all());
        $compra->save();

        return Response()->json($compra, 200);

    }

    public function delete($id)
    {
        $compra = Compra::findOrFail($id);
        $compra->delete();

        return Response()->json($compra, 201);

    }

}

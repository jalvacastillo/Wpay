<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Requests\GastoRequest;
use App\Models\Gasto;

class GastoController extends Controller
{
    

    public function index() {

        $gastos = Gasto::orderBy('id','dsc')->paginate(15);

        return Response()->json($gastos, 200);            

    }


    public function read($id) {

        $gasto = Gasto::find($id);
        return Response()->json($gasto, 200);

    }


    public function store(GastoRequest $request)
    {

        if($request->id){
            $gasto = Gasto::find($request->id);
        }
        else{
            $gasto = new Gasto;
        }
        
        $gasto->fill($request->all());
        $gasto->save();

        return Response()->json($gasto, 200);

    }

    public function delete($id)
    {
        
        $gasto = Gasto::find($id);
        $gasto->delete();

        return Response()->json($gasto, 201);


    }

}

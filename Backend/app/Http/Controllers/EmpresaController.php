<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Requests\EmpresaRequest;
use App\Models\Empresa;

class EmpresaController extends Controller
{
    

    public function index() {

        $empresas = Empresa::orderBy('id','dsc')->paginate(7);

        return Response()->json($empresas, 200);


    }


    public function read($id) {

        $empresa = Empresa::find($id);
        return Response()->json($empresa, 200);

    }

    public function search($txt) {

        $empresas = Empresa::where('nombre', 'like' ,'%' . $txt . '%')->paginate(7);
        return Response()->json($empresas, 200);

    }


    public function store(EmpresaRequest $request)
    {

        if($request->id){
            $empresa = Empresa::find($request->id);
        }
        else{
            $empresa = new Empresa;
        }
        
        $empresa->fill($request->all());
        $empresa->save();

        return Response()->json($empresa, 200);


    }

    public function delete($id)
    {

        $empresa = Empresa::find($id);
        $empresa->delete();

        return Response()->json($empresa, 201);


    }

}

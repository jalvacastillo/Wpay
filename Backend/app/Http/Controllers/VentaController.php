<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Requests\VentaRequest;
use Illuminate\Http\Request;
use App\Models\Venta;

use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class VentaController extends Controller
{
    

    public function index() {
       
        $ventas = Venta::orderBy('id','dsc')->paginate(15);

        return Response()->json($ventas, 200);
            
    }


    public function read($id) {

        $venta = Venta::find($id);
        return Response()->json($venta, 200);
            
    }

    public function search($txt) {

        $ventas = Venta::where('nombre', 'like' ,'%' . $txt . '%')->paginate(7);
        return Response()->json($ventas, 200);
            
    }

    public function filter(Request $request) {

        $ventas = Venta::when($request->inicio, function($query) use ($request){
                            return $query->where('fecha', '>=', $request->inicio);
                        })
                        ->when($request->fin, function($query) use ($request){
                            return $query->where('fecha', '<=', $request->fin);
                        })
                        ->when($request->cliente_id, function($query) use ($request){
                            return $query->where('cliente_id', $request->cliente_id);
                        })
                        ->when($request->usuario_id, function($query) use ($request){
                            return $query->where('usuario_id', $request->usuario_id);
                        })
                        ->when($request->estado, function($query) use ($request){
                            return $query->where('estado', $request->estado);
                        })
                        ->when($request->deben !== null, function($query) use ($request){
                            return $query->whereHas('detalles', function($q)
                                    {
                                        $q->where('pago_pendiente','>', 0);
                                    });
                        })
                        ->when($request->tipo, function($query) use ($request){
                            return $query->where('tipo', $request->tipo);
                        })
                        ->orderBy('id','desc')->paginate(100000);

        return Response()->json($ventas, 200);

    }


    public function store(VentaRequest $request)
    {

        if($request->id){
            $venta = Venta::find($request->id);
        }
        else{
            $venta = new Venta;
            $venta->estado = 1;
            if ($request->tipo == 'Recibo') {
                $v = Venta::where('tipo', 'Recibo')->orderBy('id', 'dsc')->first();
                if ($v) {
                    $venta->codigo = $v->codigo + 1;
                }else{
                    $venta->codigo = 1;
                }
            }
        }
        
        $venta->fill($request->all());
        $venta->save();

        return Response()->json($venta, 200);

    }

    public function delete($id)
    {

        $venta = Venta::find($id);
        $venta->delete();

        return Response()->json($venta, 201);

    }

}

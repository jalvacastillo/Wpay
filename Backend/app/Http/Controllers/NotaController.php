<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Requests\NotaRequest;
use App\Models\Nota;

class NotaController extends Controller
{
    

    public function index() {

        $notas = Nota::orderBy('id','dsc')->paginate(7);

        return Response()->json($notas, 200);            

    }


    public function read($id) {

        $nota = Nota::find($id);
        return Response()->json($nota, 200);

    }


    public function store(NotaRequest $request)
    {

        if($request->id){
            $nota = Nota::find($request->id);
        }
        else{
            $nota = new Nota;
        }
        
        $nota->fill($request->all());
        $nota->save();

        return Response()->json($nota, 200);

    }

    public function delete($id)
    {
        
        $nota = Nota::find($id);
        $nota->delete();

        return Response()->json($nota, 201);


    }

}

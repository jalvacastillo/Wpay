<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Requests\ClienteRequest;
use App\Models\Cliente;
use App\Models\Venta;

class ClienteController extends Controller
{
    

    public function index() {

        $clientes = Cliente::orderBy('id','dsc')->paginate(15);

        return Response()->json($clientes, 200);            

    }

    public function list() {

        $clientes = Cliente::orderBy('nombre','dsc')->get();

        return Response()->json($clientes, 200);            

    }

    public function ventas($cliente) {

        $ventas = Cliente::where('id', $cliente)->with('ventas')->orderBy('id','dsc')->first();

        return Response()->json($ventas, 200);

    }


    public function read($id) {

        $cliente = Cliente::find($id);
        return Response()->json($cliente, 200);

    }

    public function search($txt) {

        $clientes = Cliente::where('nombre', 'like' ,'%' . $txt . '%')->paginate(7);
        return Response()->json($clientes, 200);

    }


    public function store(ClienteRequest $request)
    {

        if($request->id){
            $cliente = Cliente::find($request->id);
        }
        else{
            $cliente = new Cliente;
        }
        
        $cliente->fill($request->all());
        $cliente->save();

        return Response()->json($cliente, 200);

    }

    public function delete($id)
    {
        
        $cliente = Cliente::find($id);
        $cliente->delete();

        return Response()->json($cliente, 201);


    }

}

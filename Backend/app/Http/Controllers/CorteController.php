<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Requests\CorteRequest;
use App\Models\Corte;

class CorteController extends Controller
{
    

    public function index() {

        $cortes = Corte::orderBy('id','dsc')->paginate(7);

        return Response()->json($cortes, 200);            

    }


    public function read($id) {

        $corte = Corte::find($id);
        return Response()->json($corte, 200);

    }


    public function store(CorteRequest $request)
    {

        if($request->id){
            $corte = Corte::find($request->id);
        }
        else{
            $corte = new Corte;
            $request['inicio'] = date('Y-m-d H:i:s');
        }
        
        $corte->fill($request->all());
        $corte->save();

        return Response()->json($corte, 200);

    }

    public function delete($id)
    {
        
        $corte = Corte::find($id);
        $corte->delete();

        return Response()->json($corte, 201);


    }

    public function cierre(CorteRequest $request)
    {
        $corte = Corte::findOrFail($request->id);
        $request['final'] = date('Y-m-d H:i:s');
        
        $corte->fill($request->all());
        $corte->save();

        return Response()->json($corte, 200);

    }

}

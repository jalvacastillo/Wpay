<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\User;

class AuthJWTController extends Controller
{
    

    public function login(Request $request){

        $credentials = $request->only('email', 'password');
        $token = null;

        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'Datos incorrectos'], 401);
        }

        $user = JWTAuth::authenticate($token);

        return response()->json(['token' => $token, 'user' => $user], 200);


    }

    public function auth(Request $request){

        $user = JWTAuth::toUser($request->token);

        if (!$user) {
            return response()->json(['user_not_found'], 404);
        }
        else{
            return response()->json($user, 200);
        }
    }


}
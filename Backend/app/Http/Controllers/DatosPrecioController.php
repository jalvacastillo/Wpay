<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\DatosPrecios;

class DatosPrecioController extends Controller
{
    

    public function index() {

        $DatosPrecios = DatosPrecios::find(1);

        return Response()->json($DatosPrecios, 200);            

    }


    public function read($id) {

        $DatosPrecios = DatosPrecios::find($id);
        return Response()->json($DatosPrecios, 200);

    }


    public function store(Request $request)
    {

        if($request->id){
            $DatosPrecios = DatosPrecios::find($request->id);
        }
        else{
            $DatosPrecios = new DatosPrecios;
        }
        
        $DatosPrecios->fill($request->all());
        $DatosPrecios->save();

        return Response()->json($DatosPrecios, 200);

    }

    public function delete($id)
    {
        
        $DatosPrecios = DatosPrecios::find($id);
        $DatosPrecios->delete();

        return Response()->json($DatosPrecios, 201);


    }


}

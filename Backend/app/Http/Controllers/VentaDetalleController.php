<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Requests\VentaDetalleRequest;
use App\Models\VentaDetalle;
use App\Models\Venta;
use App\Models\Entrada;
use Carbon\Carbon;

class VentaDetalleController extends Controller
{
    

    public function index($venta) {
       
        $venta = Venta::where('id',$venta)->with('detalles')->orderBy('id','dsc')->first();

        return Response()->json($venta, 200);


    }


    public function read($id) {

        $venta = Venta::where('id', $id)->with('detalles')->first();
        return Response()->json($venta, 200);
            
    }

    public function search($txt) {

        $ventasDetalles = VentaDetalle::where('nombre', 'like' ,'%' . $txt . '%')->paginate(7);
        return Response()->json($ventasDetalles, 200);

    }


    public function store(VentaDetalleRequest $request)
    {

        if($request->id){
            $ventaDetalle = VentaDetalle::find($request->id);

            if (  ($request->abono > $ventaDetalle->abono) && (Carbon::today() > $ventaDetalle->created_at) ) {
                $entrada = new Entrada;
                $entrada->fecha         = date('Y-m-d');
                $entrada->descripcion   = 'Abono de venta';
                $entrada->total         = $request->abono - $ventaDetalle->abono;
                $entrada->usuario_id    = 1;
                $entrada->empresa_id    = 1;
                $entrada->save();
            }

        }
        else{
            $ventaDetalle = new VentaDetalle;
        }
        
        $ventaDetalle->fill($request->all());
        $ventaDetalle->save();

        $venta = Venta::where('id', $request->venta_id)->with('detalles')->first();
        $venta->total = $venta->detalles()->sum('total');
        $venta->save();

        return Response()->json($ventaDetalle, 200);

    }

    public function delete($id)
    {

        $ventaDetalle = VentaDetalle::find($id);
        $ventaDetalle->delete();

        $venta = Venta::where('id', $ventaDetalle->venta_id)->with('detalles')->first();
        $venta->total = $venta->detalles()->sum('total');
        $venta->save();


        return Response()->json($venta, 201);

    }

}

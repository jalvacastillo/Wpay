<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Requests\ProductoRequest;
use App\Models\Producto;

class ProductosController extends Controller
{
    

    public function index() {

        $gastos = Producto::orderBy('id','dsc')->paginate(15);

        return Response()->json($gastos, 200);            

    }

    public function search($txt) {

        $productos = Producto::where('nombre', 'like' ,'%' . $txt . '%')->paginate(10);
        return Response()->json($productos, 200);

    }


    public function read($id) {

        $gasto = Producto::find($id);
        return Response()->json($gasto, 200);

    }


    public function store(ProductoRequest $request)
    {

        if($request->id){
            $gasto = Producto::find($request->id);
        }
        else{
            $gasto = new Producto;
        }
        
        $gasto->fill($request->all());
        $gasto->save();

        return Response()->json($gasto, 200);

    }

    public function delete($id)
    {
        
        $gasto = Producto::find($id);
        $gasto->delete();

        return Response()->json($gasto, 201);


    }

}

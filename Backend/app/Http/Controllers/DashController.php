<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use JWTAuth;
use Carbon\Carbon;
use stdClass;

use App\Models\Venta;
use App\Models\Nota;
use App\Models\Caja;
use App\Models\Corte;
use App\Models\Compra;
use App\Models\Gasto;
use App\Models\Entrada;
use App\Models\VentaDetalle;
use App\User;

class DashController extends Controller
{

    public function today() {
        // $empresa_id = JWTAuth::parseToken()->authenticate()->empresa_id;
        $datos = new stdClass();            

        $mes = date('m');
        $ano = date('Y');
        $dia = date('d');
        $fecha = date('Y-m-d');

        $actividades = collect();

        $caja_id = JWTAuth::parseToken()->authenticate()->caja_id;

        $datos->caja                = Caja::findOrFail($caja_id);
        $datos->corte               = Corte::where('caja_id', $caja_id)//->where('created_at', '>=', Carbon::today())
                                                                                ->orderBy('id', 'desc')
                                                                                ->first();
        // $ventasPendientes = Venta::whereIn('estado', ['Orden', 'En Proceso'])->orderBy('id', 'dsc')->get();

        if ($datos->corte) {
            $ventas = Venta::where('created_at', '>=', $datos->corte->inicio)->get();
            $compras = Compra::where('created_at', '>=', $datos->corte->inicio)->get();
            $gastos = Gasto::where('created_at', '>=', $datos->corte->inicio)->orderBy('id', 'dsc')->get();
            $entradas = Entrada::where('created_at', '>=', $datos->corte->inicio)->orderBy('id', 'dsc')->get();
            $notas = Nota::where('estado', ['Pendiente'])->orderBy('id', 'dsc')->get();

        } else {
            $ventas = Venta::where('created_at', '>=', Carbon::today())->get();
            $compras = Compra::where('created_at', '>=', Carbon::today())->get();
            $gastos = Gasto::where('created_at', '>=', Carbon::today())->orderBy('id', 'dsc')->get();
            $entradas = Entrada::where('created_at', '>=', Carbon::today())->orderBy('id', 'dsc')->get();
            $notas = Nota::where('estado', ['Pendiente'])->orderBy('id', 'dsc')->get();
        }

        foreach ($ventas as $venta) {
            $actividades->push([
                'fecha' => $venta->created_at,
                'descripcion' => 'Venta',
                'entrada' => $venta->pagado,
                'estado' => $venta->estado,
                'tipo'    => 'Venta',
                'tipoVenta'    => $venta->tipo,
                'id'    => $venta->id
            ]);
        }

        foreach ($entradas as $entrada) {
            $actividades->push([
                'fecha' => $entrada->created_at,
                'descripcion' => $entrada->descripcion,
                'entrada' => $entrada->total,
                'estado' => 'Cobrado',
                'tipo'    => 'Entrada',
                'id'    => $entrada->id
            ]);
        }

        foreach ($notas as $nota) {
            $actividades->push([
                'fecha' => $nota->created_at,
                'descripcion' => $nota->descripcion,
                'estado' => $nota->estado,
                'tipo'    => 'Nota',
                'id'    => $nota->id
            ]);
        }

        foreach ($gastos as $gasto) {
            $actividades->push([
                'fecha' => $gasto->created_at,
                'descripcion' => $gasto->descripcion,
                'salida' => $gasto->total,
                'estado' => 'Pagado',
                'tipo'    => 'Salida',
                'id'    => $gasto->id
            ]);
        }

        $datos->actividades = $actividades->sortByDesc('fecha')->values()->all();
        $datos->fecha = $fecha;
        $datos->venta = $ventas->sum('pagado');
        $datos->salida = $gastos->sum('total') + $compras->sum('total');
        $datos->entrada = $entradas->sum('total') + $datos->venta;

        $datos->balance = $datos->entrada - $datos->salida;

                                                                                
        return Response()->json($datos, 200);

    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Venta extends Model
{
    use SoftDeletes;
    protected $table = 'ventas';
    protected $fillable = [
        'fecha',
        'correlativo',
        'cliente_id',
        'estado',
        'tipo',
        'iva_retenido',
        'iva',
        'subtotal',
        'total',
        'usuario_id',
        'empresa_id'
    ];

    protected $appends = ['cliente', 'pagado', 'pendiente'];

    public function getClienteAttribute(){
        return $this->cliente()->pluck('nombre')->first();
    }

    public function getPagadoAttribute(){

        return $this->detalles()->selectRaw('SUM(abono * cantidad) AS total')->pluck('total')->first();
    }

    public function getPendienteAttribute(){
        return $this->detalles()->sum('pago_pendiente');
    }

    public function cliente(){
        return $this->belongsTo('App\Models\Cliente', 'cliente_id');
    }

    public function detalles(){
        return $this->hasMany('App\Models\VentaDetalle', 'venta_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VentaDetalle extends Model
{

    protected $table = 'ventas_detalle';
    protected $fillable = [
        'cantidad',
		'descripcion',
        'pago_pendiente',
        'abono',
		'precio',
        'total',
		'pago_total',
		'venta_id'
    ];

    public function venta(){
    	return $this->belongTo('App\Models\Venta', 'venta_id');
    }

}

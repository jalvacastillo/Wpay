<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    use SoftDeletes;
    protected $table = 'clientes';
    protected $fillable = [
        'nombre',
        'nit',
        'dui',
        'nrc',
        'giro',
        'telefono',
        'email',
        'direccion',
        'municipio',
        'departamento',
        'empresa_id'
    ];

    protected $appends = ['compras'];

    public function getComprasAttribute(){
        return $this->ventas()->count();
    }

    public function ventas(){
        return $this->hasMany('App\Models\Venta', 'cliente_id');
    }

    

}

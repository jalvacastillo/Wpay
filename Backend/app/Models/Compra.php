<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Compra extends Model {

    use SoftDeletes;
    protected $table = 'compras';
    protected $fillable = [
        'fecha',
        'proveedor',
        'descripcion',
        'nota',
        'total',
        'usuario_id',
        'empresa_id'
    ];

    public function usuario(){
        return $this->belongsTo('App\User');
    }

    public function empresa(){
        return $this->belongsTo('App\Models\Empresa');
    }

}

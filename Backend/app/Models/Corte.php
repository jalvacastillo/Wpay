<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Venta;

class Corte extends Model {

    use SoftDeletes;
    protected $table = 'cortes';
    protected $fillable = array(
        'total',
        'inicio',
        'final',
        'estado',
        'caja_id',
        'usuario_id'
    );
    protected $appends = ['usuario', 'venta'];

    public function getUsuarioAttribute(){
        return $this->usuario()->pluck('name')->first();
    }

    public function getVentaAttribute(){
        if (!$this->final) {
            $this->final = date('Y-m-d H:i:s');
        }
        
        $ventas = Venta::whereBetween('created_at', [$this->inicio, $this->final] )->get();

        return $ventas->sum('total');
    }

    public function usuario(){
        return $this->belongsTo('App\User', 'usuario_id');
    }

    public function caja(){
        return $this->belongsTo('App\Caja', 'caja_id');
    }


}




<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Caja extends Model {

    use SoftDeletes;
    protected $table = 'cajas';
    protected $fillable = [
        'nombre',
        'correlativo_credito',
        'correlativo_factura',
        'empresa_id'
    ];

    protected $appends = ['total'];

    public function getTotalAttribute(){
        return $this->cortes()->pluck('total')->last();
    }

    public function cortes(){
        return $this->hasMany('App\Models\Corte');
    }

    public function usuario(){
        return $this->belongsTo('App\User');
    }

    public function empresa(){
        return $this->belongsTo('App\Models\Empresa');
    }

}

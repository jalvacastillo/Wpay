<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DatosPrecios extends Model {

    use SoftDeletes;
    protected $table = 'datos_precios';
    protected $fillable = [
        'lona',
        'vinil',
        'diseno',
        'troquelado',
        'tornillos',
        'pega',
        'silicon',
        'laminador',
    ];


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gasto extends Model {

    use SoftDeletes;
    protected $table = 'gastos';
    protected $fillable = [
        'fecha',
        'descripcion',
        'total',
        'usuario_id',
        'empresa_id'
    ];

    public function usuario(){
        return $this->belongsTo('App\User');
    }

    public function empresa(){
        return $this->belongsTo('App\Models\Empresa');
    }

}

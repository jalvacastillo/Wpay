<?php 

// Clientes

    Route::get('/clientes',         		'ClienteController@index');
    Route::get('/clientes/buscar/{text}',	'ClienteController@search');
    Route::get('/clientes/list',   'ClienteController@list');
    Route::post('/cliente',         		'ClienteController@store');
    Route::get('/cliente/ventas/{id}',    	'ClienteController@ventas');
    Route::get('/cliente/{id}',     		'ClienteController@read');
    Route::delete('/cliente/{id}',  		'ClienteController@delete');


?>

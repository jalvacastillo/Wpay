<?php 

// Clientes

    Route::get('/ventas',         		'VentaController@index');
    Route::get('/ventas/buscar/{text}',	'VentaController@search');
    Route::post('/venta',         		'VentaController@store');
    Route::get('/venta/{id}',     		'VentaController@read');
    Route::delete('/venta/{id}',  		'VentaController@delete');

    Route::get('/venta/detalles/{id}',  'VentaDetalleController@index');
    Route::post('/venta/detalle',       'VentaDetalleController@store');
    Route::get('/venta/detalle/{id}',   'VentaDetalleController@read');
    Route::delete('/venta/detalle/{id}','VentaDetalleController@delete');

    Route::get('/ventas/deben','VentaController@deben');
    Route::get('/ventas/tipo/{tipo}','VentaController@tipo');
    Route::get('/ventas/estado/{estado}','VentaController@estado');

    Route::post('/ventas/filtrar','VentaController@filter');


?>

<?php 

// Dash

	Route::get('/dash/today',   'DashController@today');
	Route::get('/dash/month',   'DashController@month');
	Route::get('/dash/year',    'DashController@year');
	Route::get('/dash/all',    	'DashController@all');



	Route::get('/datos-precios',         		'DatosPrecioController@index');
	Route::post('/datos-precio',         		'DatosPrecioController@store');
	Route::get('/datos-precio/{id}',     		'DatosPrecioController@read');
	Route::delete('/cliedatos-precio/{id}',  		'DatosPrecioController@delete');
<?php 

// Otros

    Route::get('/gastos',         		'GastoController@index');
    Route::post('/gasto',         		'GastoController@store');
    Route::get('/gasto/{id}',     		'GastoController@read');
    Route::delete('/gasto/{id}',  		'GastoController@delete');

    Route::get('/entradas',         		'EntradaController@index');
    Route::post('/entrada',         		'EntradaController@store');
    Route::get('/entrada/{id}',     		'EntradaController@read');
    Route::delete('/entrada/{id}',  		'EntradaController@delete');

    Route::get('/notas',         		'NotaController@index');
    Route::post('/nota',         		'NotaController@store');
    Route::get('/nota/{id}',     		'NotaController@read');
    Route::delete('/nota/{id}',  		'NotaController@delete');


?>
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/api/pdf/venta/{id}', function($id){

	$venta = App\Models\Venta::where('id', $id)->with('detalles', 'cliente')->first();

	$partes = explode('.', strval( number_format($venta->total, 2) ));
	$venta->total_letras = \NumeroALetras::convertir($partes[0], 'Dolares con ') . $partes[1].'/100';

	if ($venta) {
		if ($venta->tipo == 'Recibo') {
		    $pdf = App::make('dompdf.wrapper');

		     return view('pdf.recibo', compact('venta'));
		}
		elseif ($venta->tipo == 'Consumidor Final') {
			$pdf = App::make('dompdf.wrapper');

			return view('pdf.factura', compact('venta'));
		}
		elseif ($venta->tipo == 'Credito Fiscal') {
			$pdf = App::make('dompdf.wrapper');

			// $pdf->loadView('pdf.credito', compact('venta'))->setPaper('letter');
			
			// return $pdf->stream();
			return view('pdf.credito', compact('venta'));
		}else{
			return "Venta sin tipo";
		}
	}else{
		return "Venta no existe";		
	}


});

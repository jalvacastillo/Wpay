<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/login', 'Auth\AuthJWTController@login');

// Route::group(['middleware' => ['jwt.auth']], function () {

	Route::post('/auth', 'Auth\AuthJWTController@auth');

	require base_path('routes/resourses/clientes.php');
	require base_path('routes/resourses/ventas.php');
	require base_path('routes/resourses/usuarios.php');
	require base_path('routes/resourses/empresas.php');
	require base_path('routes/resourses/compras.php');
	require base_path('routes/resourses/producto.php');
	require base_path('routes/resourses/otros.php');
	require base_path('routes/resourses/dash.php');

// });

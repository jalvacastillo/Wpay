<?php 

// Usuarios

    Route::get('/usuarios',         		'UsuarioController@index');
    Route::get('/usuarios/buscar/{text}',	'UsuarioController@search');
    Route::post('/usuario',         		'UsuarioController@store');
    Route::get('/usuarios/list',                 'UsuarioController@list');
    Route::get('/usuario/{id}',     		'UsuarioController@read');
    Route::delete('/usuario/{id}',  		'UsuarioController@delete');

    Route::get('/cajas',                'CajaController@index');
    Route::get('/cajas/buscar/{text}',  'CajaController@search');
    Route::post('/caja',                'CajaController@store');
    Route::get('/caja/{id}',            'CajaController@read');
    Route::delete('/caja/{id}',         'CajaController@delete');

    Route::get('/cortes',         		'CorteController@index');
    Route::get('/cortes/buscar/{text}',	'CorteController@search');
    Route::post('/corte',         		'CorteController@store');
    Route::get('/corte/{id}',     		'CorteController@read');
    Route::delete('/corte/{id}',  		'CorteController@delete');
    Route::post('/corte-cierre',        'CorteController@cierre');


?>

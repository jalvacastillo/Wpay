<?php 

// Productos

    Route::get('/productos',                 'ProductosController@index');
    Route::get('/productos/buscar/{text}',   'ProductosController@search');
    Route::post('/producto',                 'ProductosController@store');
    Route::get('/producto/ventas/{id}',      'ProductosController@ventas');
    Route::get('/producto/{id}',             'ProductosController@read');
    Route::delete('/producto/{id}',          'ProductosController@delete');


?>

<?php 

// Compras

    Route::get('/compras',         			'ComprasController@index');
    Route::get('/compras/buscar/{text}',	'ComprasController@search');
    Route::post('/compra',         		'ComprasController@store');
    Route::get('/compra/{id}',     		'ComprasController@read');
    Route::delete('/compra/{id}',  		'ComprasController@delete');

?>
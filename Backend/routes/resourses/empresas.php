<?php 

// Empresas

    Route::get('/empresas',         		'EmpresaController@index');
    Route::get('/empresas/buscar/{text}',	'EmpresaController@search');
    Route::post('/empresa',         		'EmpresaController@store');
    Route::get('/empresa/{id}',     		'EmpresaController@read');
    Route::delete('/empresa/{id}',  		'EmpresaController@delete');


?>
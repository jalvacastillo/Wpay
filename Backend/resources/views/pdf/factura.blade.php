<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="{{ asset('css/print.css') }}">

	<title>Factura</title>
	<style>
		#fecha{position: absolute; top: 3.7cm; left: 9.5cm; font-size: 25px; }

		#cliente{position: absolute; top: 4.8cm; left: 2cm; font-size: 12px; width: 11cm; text-wrap: nowrap; overflow: hidden;}
		#direccion{position: absolute; top: 5.5cm; left: 2.5cm; width: 6cm; text-wrap: nowrap; overflow: hidden; font-size: 12px; }
		#nit{position: absolute; top: 6cm; left: 2cm; font-size: 12px; }

		#suma{position: absolute; top: 15.7cm; left: 11.3cm; font-size: 12px; }
		#retenido{position: absolute; top: 16.4cm; left: 11.3cm; font-size: 12px; }
		#nosujeta{position: absolute; top: 17.1cm; left: 11.3cm; font-size: 12px; }
		#excenta{position: absolute; top: 17.8cm; left: 11.3cm; font-size: 12px; }
		#total{position: absolute; top: 19cm; left: 11.3cm; font-size: 12px; }
		#letras{position: absolute; top: 16.5cm; left: 2cm; font-size: 12px; }

		table{position: absolute; top: 7.5cm; left: 0.6cm; text-align: left; font-size: 12px; border-collapse: collapse;}
		table td{height: 0.7cm;}
	</style>

</head>
<body onload="javascript:print();" style="background-image: url('/backend/img/recibo1.JPG'); background-size: 810px; background-repeat: no-repeat;">
		
	<section>
		<p id="fecha">{{ $venta->created_at->format('d   m   y') }}</p>
		<p id="cliente">{{ $venta->cliente }}</p>
		<p id="direccion">{{ $venta->cliente()->pluck('direccion')->first() }} {{ $venta->cliente()->pluck('departamento')->first() }}</p>
		<p id="nit">{{ $venta->cliente()->pluck('nit')->first() }} {{ $venta->cliente()->pluck('dui')->first() }}</p>
		
			
			<table>
				@foreach($venta->detalles as $detalle)
				<tr>
					<td width="45px" class="text-center">{{ $detalle->cantidad }}</td>
					<td width="240px">{{ $detalle->descripcion }}</td>
					<td width="50px" class="text-center">${{ number_format($detalle->precio, 2) }}</td>
					<td width="30px"></td>
					<td width="45px"></td>
					<td width="30px" class="text-center">${{ number_format($detalle->cantidad * $detalle->precio, 2) }}</th>
				</tr>
				@endforeach
			</table>
		<p id="suma">$ {{ number_format($venta->total, 2) }}</p>
		<p id="retenido">$ {{ number_format($venta->iva_retenido, 2) }}</p>
		<p id="total">$ {{ number_format($venta->total, 2) }}</p>
		<p id="letras">{{ $venta->total_letras }}</p>
		<button id="p" onClick="window.close();" autofocus>Cerrar</button>
	</section>


</div>
</body>
</html>

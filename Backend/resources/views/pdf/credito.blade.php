<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="{{ asset('css/print.css') }}">

	<title>Credito</title>
	<style>
		#fecha{position: absolute; top: 3.7cm; left: 9.3cm; font-size: 25px; }

		#cliente{position: absolute; top: 4.8cm; left: 2cm; font-size: 12px; width: 11cm; text-wrap: nowrap; overflow: hidden;}
		#direccion{position: absolute; top: 5.3cm; left: 2.5cm; width: 6cm; text-wrap: nowrap; overflow: hidden; font-size: 12px; }
		#departamento{position: absolute; top: 5.3cm; left: 10cm; font-size: 12px; }
		#nit{position: absolute; top: 5.8cm; left: 2cm; font-size: 12px; }
		#nrc{position: absolute; top: 5.8cm; left: 8cm; font-size: 12px; }
		#giro{position: absolute; top: 6.3cm; left: 2cm; font-size: 12px; }

		#suma{position: absolute; top: 16.5cm; left: 11.3cm; font-size: 12px; }
		#iva{position: absolute; top: 17cm; left: 11.3cm; font-size: 12px; }
		#subtotal{position: absolute; top: 17.5cm; left: 11.3cm; font-size: 12px; }
		#retenido{position: absolute; top: 18cm; left: 11.3cm; font-size: 12px; }
		#nosujeta{position: absolute; top: 18.5cm; left: 11.3cm; font-size: 12px; }
		#excenta{position: absolute; top: 19cm; left: 11.3cm; font-size: 12px; }
		#total{position: absolute; top: 19.5cm; left: 11.3cm; font-size: 12px; }
		#letras{position: absolute; top: 17cm; left: 2cm; font-size: 12px; }

		table{position: absolute; top: 8.6cm; left: 0.6cm; text-align: left; font-size: 12px; border-collapse: collapse;}
		table td{height: 0.7cm;}
	</style>

</head>
<body onload="javascript:print();">
		
	<section style="position: relative;">
		<p id="fecha">{{ $venta->created_at->format('d   m   y') }}</p>
		<p id="cliente">{{ $venta->cliente }}</p>
		<p id="direccion">{{ $venta->cliente()->first()->direccion }}</p>
		<p id="departamento">{{ $venta->cliente()->first()->departamento }}</p>
		<p id="nit">{{ $venta->cliente()->first()->nit }}</p>
		<p id="nrc">{{ $venta->cliente()->first()->nrc }}</p>
		<p id="giro">{{ $venta->cliente()->first()->giro }}</p>
		
			
			<table>
				@foreach($venta->detalles as $detalle)
				<tr>
					<td style="width:1cm;" class="text-center">{{ $detalle->cantidad }}</td>
					<td style="width:6.5cm;">{{ $detalle->descripcion }}</td>
					<td style="width:1.2cm;" class="text-center">${{ number_format(($detalle->precio / 1.13), 2) }}</td>
					<td style="width:0.8cm;"></td>
					<td style="width:0.8cm;"></td>
					<td style="width:1.5cm;" class="text-center">${{ number_format($detalle->cantidad * ($detalle->precio / 1.13), 2) }}</th>
				</tr>
				@endforeach
			</table>
		<p id="suma">$ {{ number_format($venta->total / 1.13, 2) }}</p>
		<p id="iva">$ {{ number_format($venta->total - ($venta->total / 1.13), 2) }}</p>
		<p id="subtotal">$ {{ number_format($venta->total, 2) }}</p>
		<p id="retenido">$ {{ number_format($venta->iva_retenido, 2) }}</p>
		<p id="total">$ {{ number_format($venta->total, 2) }}</p>
		<p id="letras">{{ $venta->total_letras }}</p>
		<button id="p" onClick="window.close();" autofocus>Cerrar</button>
	</section>


</div>
</body>
</html>

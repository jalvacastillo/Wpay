﻿<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="{{ asset('css/print.css') }}">

	<title>Recibo</title>
	<style>
		table td{height: 20px;}
		h1{font-size: 40px;}
		h2{font-size: 30px;}
		.text-danger{color: red;}
		p{font-size: 16px;}
		.footer p{font-size: 12px; margin: 10px 5px;}
	</style>

</head>
<body onload="javascript:print();" style="width: 500px;">
	<header>
		<table>
			<tr>
				<td width="200px" class="text-center"> <img src="{{ asset('img/logo.jpg') }}" height="100px" class="logo" /></td>
				<td width="200px" class="text-center">
					<h1 style="margin-bottom: 0px;">Recibo</h1>
					<p style="font-size: 10px;">Tel.: (503)7450-3375</p>
					<p style="font-size: 10px;">publicidadeinformatica@gmail.com</p>
				</td>
				<td width="200px" class="text-center text-danger"><h2>N° {{ $venta->codigo }}</h2></td>
			</tr>
		</table>
	</header>
	
	<section>
		<table>
			<tr>
				<td width="300px" class="text-center">
					<p><b>Por:</b> ${{ number_format($venta->detalles[0]->precio, 2) }}</p>
				</td>
				<td width="300px" class="text-center">
					<p class="rigth"><b>Fecha:</b> {{ $venta->fecha }}</p>
				</td>
			</tr>
		</table>
		<br>
		<table>
			<tr>
				<td class="text-left">
					<p><b>Recibimos de:</b> {{ $venta->cliente }}</p>
				</td>
			</tr>
			<tr>
				<td class="text-left">
					<p><b>Por concepto de:</b></p>
					<p>{{ $venta->detalles[0]->descripcion }}</p>
				</td>
			</tr>
		</table>
		<table>
			<tr>
				<td width="200px">
					<p><b>Cancelación Total:</b> {{ $venta->detalles[0]->pago_total == 1 ? 'Si' : 'No'}}</p>
				</td>
				@if( $venta->detalles[0]->pago_total != 1)
				<td width="200px">
					<p><b>Abona:</b> $ {{ $venta->detalles[0]->abono }}</p>
				</td>
				<td width="200px">
					<p><b>Resta:</b> $ {{ $venta->detalles[0]->pago_pendiente }}</p>
				</td>
				@endif
			</tr>
		</table>

	</section>
	<table class="footer">
		<tr>
			<td width="300px">
				<p>Firma:___________________________</p>
				<p>Recibe:___________________________</p>
				<p>DUI:___________________________</p>
			</td>
			<td width="300px" class="text-center">
				<br>
				<p>Recibido Global (Sello)</p>
				<p class="text-danger">Original-Emisor Duplicado-Cliente</p>
			</td>
		</tr>
	</table>
	
	<button id="p" onClick="window.close();" autofocus>Cerrar</button>

</div>
</body>
</html>






